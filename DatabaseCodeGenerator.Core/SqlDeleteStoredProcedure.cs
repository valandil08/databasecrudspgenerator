﻿using DevLab.Standard.Core.Connections;
using DevLab.Standard.Core.Connections.SQL;
using DevLab.Standard.Integration.DatabaseInfo;
using System;
using System.Text;

namespace DatabaseCodeGenerator.Core
{
    public class SqlDeleteStoredProcedure : SqlCodeGenerator, ISqlCodeGenerator
    {
        protected override string SqlServerCreateCode(ISqlDb sqlDatabase, DbTableInfo tableInfo, bool useCrudSchema)
        {
            string spName = GetName(tableInfo, "uspDelete");
            string viewName = GetName(tableInfo, "vw");

            string sqlFile = EmbeddedResource.GetResourceSqlFile(Environment.GetEnvironmentVariable("ProgramDir"), "SqlServerCreateDeleteStoredProcedure");

            sqlFile = sqlFile.Replace("[SpName]", spName);

            if (useCrudSchema)
            {
                sqlFile = sqlFile.Replace("[StoredProcedureSchemaName]", "Crud");
            }
            else
            {
                sqlFile = sqlFile.Replace("[StoredProcedureSchemaName]", tableInfo.Schema);
            }

            sqlFile = sqlFile.Replace("[ViewSchemaName]", tableInfo.Schema);

            
            sqlFile = sqlFile.Replace("[WhereClause]", GenerateWhereClause(tableInfo));
            sqlFile = sqlFile.Replace("[Parameters]", GenerateParameters(tableInfo));

            sqlFile = sqlFile.Replace("[ViewName]", viewName);


            return sqlFile;
        }

        protected override string SqlServerRemoveCode(ISqlDb sqlDatabase, DbTableInfo tableInfo, bool useCrudSchema)
        {
            string spName = GetName(tableInfo, "uspDelete");

            string sqlFile = EmbeddedResource.GetResourceSqlFile(Environment.GetEnvironmentVariable("ProgramDir"), "SqlServerRemoveStoredProcedure");

            if (useCrudSchema)
            {
                sqlFile = sqlFile.Replace("[StoredProcedureSchemaName]", "Crud");
            }
            else
            {
                sqlFile = sqlFile.Replace("[StoredProcedureSchemaName]", tableInfo.Schema);
            }

            sqlFile = sqlFile.Replace("[SpName]", spName);

            return sqlFile;
        }

        private string GenerateWhereClause(DbTableInfo tableInfo)
        {
            StringBuilder sb = new StringBuilder();

            bool isFirst = true;

            foreach (DbColumnInfo column in tableInfo.Columns)
            {
                if (!isFirst)
                {
                    sb.Append(Environment.NewLine + "	AND	  ");
                }
                else
                {
                    isFirst = false;
                }

                if (column.IsNullable)
                {
                    sb.AppendLine("	(");
                    sb.AppendLine("			[ViewName]." + column.Name + " = COALESCE(@" + column.Name + ",[ViewName]." + column.Name + ") OR");
                    sb.AppendLine("			(");
                    sb.AppendLine("				[ViewName]." + column.Name + " IS NULL AND @" + column.Name + " IS NULL ");
                    sb.AppendLine("			)");
                    sb.Append("	)");
                }
                else
                {
                    sb.Append("	[ViewName]." + column.Name + " = COALESCE(@" + column.Name + ",[ViewName]." + column.Name + ")");
                }
            }

            return sb.ToString();
        }

        private string GenerateParameters(DbTableInfo tableInfo)
        {
            StringBuilder sb = new StringBuilder();

            bool isFirst = true;

            foreach (DbColumnInfo column in tableInfo.Columns)
            {
                if (!isFirst)
                {
                    sb.AppendLine(",");
                }
                else
                {
                    isFirst = false;
                }

                sb.Append("	@" + column.Name + " " + column.DataType + " = NULL");

            }

            return sb.ToString();
        }
    }
}
