﻿using DevLab.Standard.Core.Connections.SQL;
using DevLab.Standard.Integration.DatabaseInfo;
using System.Collections.Generic;

namespace DatabaseCodeGenerator.Core
{
    public interface ISqlCodeGenerator
    {
        string Create(ISqlDb sqlDatabase, DbTableInfo tableInfo, bool useCrudSchema);
        string Remove(ISqlDb sqlDatabase, DbTableInfo tableInfo, bool useCrudSchema);
    }
}
