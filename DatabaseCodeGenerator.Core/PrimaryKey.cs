﻿using DevLab.Standard.Core.Connections.SQL;
using DevLab.Standard.Integration.DatabaseInfo;
using System;
using System.Collections.Generic;

namespace DatabaseCodeGenerator.Core
{
    public class PrimaryKey : SqlCodeGenerator, ISqlCodeGenerator
    {
        protected override string SqlServerCreateCode(ISqlDb sqlDatabase, DbTableInfo tableInfo, bool useCrudSchema)
        {
            throw new NotImplementedException();
        }

        protected override string SqlServerRemoveCode(ISqlDb sqlDatabase, DbTableInfo tableInfo, bool useCrudSchema)
        {
            throw new NotImplementedException();
        }
    }
}
