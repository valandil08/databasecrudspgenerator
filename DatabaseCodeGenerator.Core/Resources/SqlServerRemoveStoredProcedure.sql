IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[StoredProcedureSchemaName].[SpName]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	DROP PROCEDURE [StoredProcedureSchemaName].[SpName]
END