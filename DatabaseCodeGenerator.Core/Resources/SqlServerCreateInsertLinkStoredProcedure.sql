CREATE PROCEDURE [StoredProcedureSchemaName].[SpName]
(	
[Parameters],
	@AllowDuplicate bit = 0
)
AS
BEGIN
	SET NOCOUNT ON;	

	[DefaultParameterValues]

	DECLARE @InsertRecord bit = 1;

	IF NOT EXISTS
	(
		SELECT 1
		FROM [ViewSchemaName].[ViewName]
		WHERE [SelectParameters] 
	)
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION trans
	
			INSERT INTO [ViewSchemaName].[ViewName]
					([ColumnNames])
				VALUES
				([InsertValues])		
	
			COMMIT TRANSACTION trans

			RETURN 1;
		END TRY
		BEGIN CATCH			
			ROLLBACK TRANSACTION trans
			RETURN 0;

			-- Log Error
		END CATCH
	END
	ELSE
	BEGIN
		RETURN 0;
	END
END
