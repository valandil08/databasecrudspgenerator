CREATE PROCEDURE [StoredProcedureSchemaName].[SpName]
(	
[Parameters]
)
AS
BEGIN
	SET NOCOUNT ON;	

	[DefaultParameterValues]
		
	BEGIN TRY
		BEGIN TRANSACTION trans
	
		INSERT INTO [ViewSchemaName].[ViewName]
		([ColumnNames])
		VALUES
		([InsertValues])		
	
		COMMIT TRANSACTION trans

		RETURN SCOPE_IDENTITY();
	END TRY
	BEGIN CATCH			
		ROLLBACK TRANSACTION trans

		-- Log Error
	END CATCH
END
