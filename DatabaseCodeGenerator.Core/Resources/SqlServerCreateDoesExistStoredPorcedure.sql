CREATE PROCEDURE  [StoredProcedureSchemaName].[SpName]
(	
[Parameters]
)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS
	(
		SELECT *
		FROM  [SchemaName].[ViewName]
		WHERE [WhereClause]
	)
	BEGIN
		RETURN 1;
	END
	ELSE
	BEGIN
		RETURN 0;
	END
END