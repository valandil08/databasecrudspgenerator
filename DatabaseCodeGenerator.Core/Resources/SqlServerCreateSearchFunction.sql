CREATE FUNCTION [FunctionSchemaName].[FunctionName]
(	
[Parameters]
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT      
	[SelectColumns]

	FROM            
	[ViewSchemaName].[ViewName] 

	WHERE 
	[WhereClause]
)