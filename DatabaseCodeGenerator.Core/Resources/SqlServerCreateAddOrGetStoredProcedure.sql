CREATE PROCEDURE [StoredProcedureSchemaName].[SpName]
(	
[Parameters]
)
AS
BEGIN
	SET NOCOUNT ON;	

	[DefaultParameterValues]
	
	DECLARE @Id int = -1;

	IF EXISTS
	(
		SELECT 1
		FROM [ViewSchemaName].[ViewName]
		WHERE [SelectParameters] 
	)
	BEGIN
		SELECT @Id = [IdentityColumn]
		FROM [ViewSchemaName].[ViewName]
		WHERE [SelectParameters] 
	END
	ELSE
	BEGIN

		EXEC @Id = [InsertSchema].[InsertSpName]
[InsertSpParameters]
             
	END

	
	RETURN @Id;
END
