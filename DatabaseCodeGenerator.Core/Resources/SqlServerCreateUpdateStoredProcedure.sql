CREATE PROCEDURE [StoredProcedureSchemaName].[SpName]
(	
[Parameters]
)

AS
BEGIN
BEGIN TRY
	BEGIN TRANSACTION Trans
	
	UPDATE [SchemaName].[ViewName]

	SET
[SetClause]

	WHERE
[WhereClause]
	
	COMMIT TRANSACTION Trans

	RETURN SCOPE_IDENTITY();
END TRY
BEGIN CATCH			
	ROLLBACK TRANSACTION Trans;

	THROW;

	-- Log Error
END CATCH
END