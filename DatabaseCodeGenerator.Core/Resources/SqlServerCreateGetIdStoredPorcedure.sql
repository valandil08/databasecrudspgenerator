CREATE PROCEDURE  [StoredProcedureSchemaName].[SpName]
(	
[Parameters]
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id INT = -1;

	SELECT  @Id = [IdentityColumn]
	FROM  [SchemaName].[ViewName]
	WHERE [WhereClause]
		
	RETURN @Id
END