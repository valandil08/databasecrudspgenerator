IF EXISTS
(
	SELECT 1
	FROM sys.views     
	WHERE schema_id = SCHEMA_ID('[ViewSchemaName]') and name = '[ViewName]' 
)
BEGIN
	DROP VIEW [ViewSchemaName].[ViewName];
END