﻿namespace DatabaseCodeGenerator
{
    partial class program
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.rtbReadout = new System.Windows.Forms.RichTextBox();
            this.btnGenerateSp = new System.Windows.Forms.Button();
            this.cbUseCrudSchema = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server Name / IP";
            // 
            // txtServerName
            // 
            this.txtServerName.Location = new System.Drawing.Point(12, 33);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(214, 22);
            this.txtServerName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Server Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(12, 177);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(214, 22);
            this.txtPassword.TabIndex = 4;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(12, 129);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(214, 22);
            this.txtUsername.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Server Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Database Name";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(12, 81);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(214, 22);
            this.txtDatabase.TabIndex = 2;
            // 
            // rtbReadout
            // 
            this.rtbReadout.Location = new System.Drawing.Point(233, -4);
            this.rtbReadout.Name = "rtbReadout";
            this.rtbReadout.Size = new System.Drawing.Size(306, 284);
            this.rtbReadout.TabIndex = 9;
            this.rtbReadout.Text = "";
            // 
            // btnGenerateSp
            // 
            this.btnGenerateSp.Location = new System.Drawing.Point(12, 232);
            this.btnGenerateSp.Name = "btnGenerateSp";
            this.btnGenerateSp.Size = new System.Drawing.Size(213, 35);
            this.btnGenerateSp.TabIndex = 10;
            this.btnGenerateSp.Text = "Generate Stored Procedures";
            this.btnGenerateSp.UseVisualStyleBackColor = true;
            this.btnGenerateSp.Click += new System.EventHandler(this.btnGenerateSp_Click);
            // 
            // cbUseCrudSchema
            // 
            this.cbUseCrudSchema.AutoSize = true;
            this.cbUseCrudSchema.Checked = true;
            this.cbUseCrudSchema.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUseCrudSchema.Location = new System.Drawing.Point(12, 205);
            this.cbUseCrudSchema.Name = "cbUseCrudSchema";
            this.cbUseCrudSchema.Size = new System.Drawing.Size(144, 21);
            this.cbUseCrudSchema.TabIndex = 11;
            this.cbUseCrudSchema.Text = "Use Crud Schema";
            this.cbUseCrudSchema.UseVisualStyleBackColor = true;
            // 
            // program
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 279);
            this.Controls.Add(this.cbUseCrudSchema);
            this.Controls.Add(this.btnGenerateSp);
            this.Controls.Add(this.rtbReadout);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDatabase);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtServerName);
            this.Controls.Add(this.label1);
            this.Name = "program";
            this.Text = "Database SP Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.RichTextBox rtbReadout;
        private System.Windows.Forms.Button btnGenerateSp;
        private System.Windows.Forms.CheckBox cbUseCrudSchema;
    }
}

