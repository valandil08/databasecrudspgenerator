﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace DatabaseCodeGenerator
{
    static class Program
    {        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Environment.SetEnvironmentVariable("ProgramDir", AppDomain.CurrentDomain.BaseDirectory);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new program());
        }

        
    }
}
