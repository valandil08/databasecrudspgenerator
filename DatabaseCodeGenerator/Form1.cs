﻿using DatabaseCodeGenerator.Core;
using DevLab.Framework.Core.Connections.SQL;
using DevLab.Standard.Core.Connections.SQL;
using DevLab.Standard.Integration.DatabaseInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseCodeGenerator
{
    public partial class program : Form
    {
        public program()
        {
            InitializeComponent();
        }

        private void btnGenerateSp_Click(object sender, EventArgs e)
        {
            rtbReadout.Clear();
            rtbReadout.AppendText("Extracting Database Schema" + Environment.NewLine);
            try
            {
                GenerateDatabase();
                rtbReadout.AppendText("Done" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                rtbReadout.Clear();
                rtbReadout.AppendText(ex.Message);
            }
        }


        private  void GenerateDatabase()
        {
            string server = txtServerName.Text;
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            string database = txtDatabase.Text;
            bool useCrud = cbUseCrudSchema.Checked;

            SqlServerDb db = new SqlServerDb();
            db.SetConnectionString("Data Source=" + server + ";Initial Catalog=" + database + ";UID=" + username + "; pwd=" + password);

            DbDatabaseInfo dbInfo = new DbDatabaseInfo(db, database);

            foreach (DbTableInfo tableInfo in dbInfo.Tables)
            {
                bool hasIdenity = GetIdentityColumn(tableInfo.Columns) != null;

                string viewRemove = new SqlView().Remove(db, tableInfo, useCrud);
                string searchFunctionRemove = new SqlSearchFunction().Remove(db, tableInfo, useCrud);
                string deleteStoredProcedureRemove = new SqlDeleteStoredProcedure().Remove(db, tableInfo, useCrud);
                string updateStoredProcedureRemove = new SqlUpdateStoredProcedure().Remove(db, tableInfo, useCrud);                
                string addStoredProcedureRemove = new SqlAddStoredProcedure().Remove(db, tableInfo, useCrud);
                string getIdStoredProcedureRemove = new SqlGetIdStoredProcedure().Remove(db, tableInfo, useCrud);
                string addOrGetStoredProcedureRemove = new SqlAddOrGetStoredProcedure().Remove(db, tableInfo, useCrud);
                string doesExistStoredProcedureRemove = new SqlDoesExistStoredProcedure().Remove(db, tableInfo, useCrud);
                string addLinkStoredProcedureCreateRemove = new SqlAddLinkStoredProcedure().Remove(db, tableInfo, useCrud);


                // remove old stored procedures
                ExecuteSQL(db, viewRemove);
                ExecuteSQL(db, searchFunctionRemove);
                ExecuteSQL(db, deleteStoredProcedureRemove);
                ExecuteSQL(db, updateStoredProcedureRemove);
                ExecuteSQL(db, addStoredProcedureRemove);
                ExecuteSQL(db, getIdStoredProcedureRemove);
                ExecuteSQL(db, doesExistStoredProcedureRemove);
                ExecuteSQL(db, addLinkStoredProcedureCreateRemove);
                ExecuteSQL(db, addOrGetStoredProcedureRemove);


                if (tableInfo.Name == "sysdiagrams")
                {
                    continue;
                }

                string viewCreate = new SqlView().Create(db, tableInfo, useCrud);
                string searchFunctionCreate = new SqlSearchFunction().Create(db, tableInfo, useCrud);
                string deleteStoredProcedureCreate = new SqlDeleteStoredProcedure().Create(db, tableInfo, useCrud);

                // create new stored procedures
                ExecuteSQL(db, viewCreate);
                ExecuteSQL(db, searchFunctionCreate);
                ExecuteSQL(db, deleteStoredProcedureCreate);

                                
                string updateStoredProcedureCreate = new SqlUpdateStoredProcedure().Create(db, tableInfo, useCrud);
                ExecuteSQL(db, updateStoredProcedureCreate);
                

                if (hasIdenity)
                {
                    string addStoredProcedureCreate = new SqlAddStoredProcedure().Create(db, tableInfo, useCrud);
                    ExecuteSQL(db, addStoredProcedureCreate);
                }
                else
                {
                    string addLinkStoredProcedureCreate = new SqlAddLinkStoredProcedure().Create(db, tableInfo, useCrud);
                    ExecuteSQL(db, addLinkStoredProcedureCreate);                    
                }

                if (hasIdenity)
                {
                    string getIdStoredProcedureCreate = new SqlGetIdStoredProcedure().Create(db, tableInfo, useCrud);
                    ExecuteSQL(db, getIdStoredProcedureCreate);
                }
                else
                {
                    string doesExistStoredProcedureCode = new SqlDoesExistStoredProcedure().Create(db, tableInfo, useCrud);
                    ExecuteSQL(db, doesExistStoredProcedureCode);
                }

                if (hasIdenity)
                {
                    string addOrGetStoredProcedureCreate = new SqlAddOrGetStoredProcedure().Create(db, tableInfo, useCrud);
                    ExecuteSQL(db, addOrGetStoredProcedureCreate);
                }
            }
        }

        private string GenerateDefaultParameterValues(List<DbColumnInfo> columns)
        {
            string sql = "";
            
            foreach (DbColumnInfo col in columns)
            {
                if (col.DefaultValue != null)
                {
                    sql += Environment.NewLine;

                    sql += "IF( @" + col.Name + " IS NULL )" + Environment.NewLine;
                    sql += "BEGIN" + Environment.NewLine;
                    sql += "SET @" + col.Name + " = " + col.DefaultValue + ";" + Environment.NewLine;
                    sql += "END" + Environment.NewLine;

                }
            }

            return sql;
        }

        private  string GetIdentityColumn(List<DbColumnInfo> columns)
        {
            foreach (DbColumnInfo column in columns)
            {
                if (column.IsIdentity)
                {
                    return "[" + column.Name + "]";
                }
            }

            return null;
        }

        private  string GenerateSetClause(List<DbColumnInfo> columns)
        {
            string sql = "";

            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if (!col.IsIdentity && !col.IsPrimarykey)
                {
                    if (!first)
                    {
                        sql += ", " + Environment.NewLine;
                    }
                    else
                    {
                        first = false;
                    }

                    sql += "[ViewName]." + col.Name + " = COALESCE(@Set" + col.Name + ",[ViewName]." + col.Name + ")";
                }
            }

            return sql;
        }

        private  string GenerateWhereClause(List<DbColumnInfo> columns, bool includeIdentites = true, bool includeCoalesce = true, bool includeNullCheck = false)
        {
            string sql = "";

            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if ((col.IsIdentity && includeIdentites) || !col.IsIdentity)
                {
                    if (!first)
                    {
                        sql += " AND " + Environment.NewLine;
                        if (includeCoalesce)
                        {
                            sql += "	";
                        }
                        else
                        {
                            sql += "			      ";
                        } 
                    }
                    else
                    {
                        first = false;
                    }
                    if (includeCoalesce)
                    {
                        if (includeNullCheck && col.IsNullable)
                        {
                            sql += "(" + Environment.NewLine;
                            sql += "		[ViewName]." + col.Name + " = COALESCE(@" + col.Name + ",[ViewName]." + col.Name + ") OR" + Environment.NewLine;
                            sql += "		(" + Environment.NewLine;
                            sql += "			[ViewName]." + col.Name + " IS NULL AND @" + col.Name+" IS NULL " + Environment.NewLine;
                            sql += "		)" + Environment.NewLine;
                            sql += "	)" + Environment.NewLine;
                        }
                        else
                        {
                            sql += "[ViewName]." + col.Name + " = COALESCE(@" + col.Name + ",[ViewName]." + col.Name + ")";
                        }
                    }
                    else
                    {
                        sql += "[ViewName]." + col.Name + " = @" + col.Name;
                    }
                }
            }

            return sql;
        }

        private  string GenerateSpParameters(List<DbColumnInfo> columns, bool includeIdentites = true)
        {
            string parameters = "";


            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if ((col.IsIdentity && includeIdentites) || !col.IsIdentity)
                {
                    if (!first)
                    {
                        parameters += "," + Environment.NewLine + "	";
                    }
                    else
                    {
                        first = false;
                    }

                    parameters += "@" + col.Name + " = " + "@" + col.Name;
                }
            }
            return parameters;
        }

        private  string GenerateParameters(List<DbColumnInfo> columns, bool includeIdentites = true)
        {
            string parameters = "";


            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if ((col.IsIdentity && includeIdentites) || !col.IsIdentity)
                {
                    if (!first)
                    {
                        parameters += "," + Environment.NewLine + "	";
                    }
                    else
                    {
                        first = false;
                    }

                    parameters += "@" + col.Name + " " + col.DataType + " = NULL";
                }
            }
            return parameters;
        }

        private  string GenerateFunctionParameterClause(List<DbColumnInfo> columns)
        {
            string parameterList = "";


            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if (!first)
                {
                    parameterList += ",";
                }
                else
                {
                    first = false;
                }

                parameterList += "@" + col.Name;
            }

            return parameterList;
        }

        private  string GenerateSelectClause(List<DbColumnInfo> columns)
        {
            string selectClause = "";

            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if (!first)
                {
                    selectClause += "," + Environment.NewLine + "	";
                }
                else
                {
                    first = false;
                }

                selectClause += col.Name;
            }

            return selectClause;
        }

        private  string GenerateInsertColumnsClause(List<DbColumnInfo> columns)
        {
            string selectClause = "";

            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if (!col.IsIdentity)
                {
                    if (!first)
                    {
                        selectClause += "," + Environment.NewLine;
                    }
                    else
                    {
                        first = false;
                    }

                    selectClause += col.Name;
                }
            }

            return selectClause;
        }

        private  string GenerateColumnNames(List<DbColumnInfo> columns, bool includeIdentites = true)
        {
            string cols = "";

            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if ((col.IsIdentity && includeIdentites) || !col.IsIdentity)
                {
                    if (!first)
                    {
                        cols += ",";
                    }
                    else
                    {
                        first = false;
                    }

                    cols += "[" + col.Name + "]";
                }
            }

            return cols;
        }

        private  string GenerateInsertValues(List<DbColumnInfo> columns, bool includeIdentites = true)
        {
            string cols = "";

            bool first = true;

            foreach (DbColumnInfo col in columns)
            {
                if ((col.IsIdentity && includeIdentites) || !col.IsIdentity)
                {
                    if (!first)
                    {
                        cols += ",";
                    }
                    else
                    {
                        first = false;
                    }

                    cols += "@" + col.Name;
                }
            }

            return cols;
        }

        private  string GenerateUpdateParameters(DbTableInfo tableInfo)
        {
            string paramsA = GenerateParameters(tableInfo.Columns, false).Replace("@", "@Set");
            string paramsB = GenerateParameters(tableInfo.Columns, true).Replace("@", "@Where");
            return paramsA + "," + Environment.NewLine + "	" + paramsB;
        }

        private  string GetResourceSqlFile(string fileName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            string baseUrl = assembly.GetName().Name;
            string resourceName = baseUrl + ".Resources." + fileName + ".sql";

            string sql = null;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    sql = reader.ReadToEnd();
                }
            }

            if (sql == null)
            {
                return "";
            }

            return sql;
        }

        private void ExecuteSQL(ISqlDb db, string sql)
        {
            db.SetSql(sql);
            db.ExecuteRequest();
        }

        private  void PopulateAndProcess(ISqlDb db, string fileName, params Tuple<string, string>[] keyPairs)
        {
            string schema = "";
            string objectName = "";
            string prefix = "Creating";

            if (fileName.Length > 6)
            {
                if (fileName.Contains("Remove"))
                {
                    prefix = "Removing";
                }
            }

            string sql = GetResourceSqlFile(fileName);

            foreach (Tuple<string, string> keyPair in keyPairs)
            {
                if (keyPair.Item1 == "[SchemaName]")
                {
                    schema = keyPair.Item2;
                }

                if (keyPair.Item1 == "[SpName]" || keyPair.Item1 == "[FunctionName]" || keyPair.Item1 == "[ViewName]")
                {
                    objectName = keyPair.Item2;
                }

                sql = sql.Replace(keyPair.Item1, keyPair.Item2);
            }




            rtbReadout.AppendText(Environment.NewLine + prefix + " " + schema + "." + objectName);

            db.SetSql(sql);

            db.ExecuteRequest();
        }

    }
}
