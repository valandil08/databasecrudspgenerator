CREATE FUNCTION [FunctionSchemaName].[FunctionName]
(	
	[Parameters]
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT      
	[SelectClause]

	FROM            
	[SchemaName].[ViewName] 

	WHERE 
	[WhereClause]
)