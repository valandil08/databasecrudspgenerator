CREATE PROCEDURE  [StoredProcedureSchemaName].[SpName]
	[Parameters],
	@InsertIfNotFound			bit					= 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id INT = -1;
	 
	IF (@InsertIfNotFound = 1)
	BEGIN			
	
		IF NOT EXISTS
		(
			SELECT  1
			FROM  [SchemaName].[ViewName]
			WHERE [WhereClause]
		)
		BEGIN	
					 
			EXEC @Id = [StoredProcedureSchemaName].[AddSpName] 
			[AddSpParameters]
					 
		END	

	END

	IF (@Id = -1)
	BEGIN

		SELECT  @Id = [IdentityColumn]
		FROM  [SchemaName].[ViewName]
		WHERE [WhereClause]

	END

	RETURN @Id
END