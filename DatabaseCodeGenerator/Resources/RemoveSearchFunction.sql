IF EXISTS
(
SELECT 1
FROM sys.objects
WHERE type IN ('FN', 'IF', 'TF') AND schema_id = SCHEMA_ID('[FunctionSchemaName]') and name = '[FunctionName]' 
)
BEGIN
	DROP FUNCTION [FunctionSchemaName].[FunctionName];
END