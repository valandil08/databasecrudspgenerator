IF EXISTS
(
	SELECT 1
	FROM sys.views     
	WHERE schema_id = SCHEMA_ID('[SchemaName]') and name = '[ViewName]' 
)
BEGIN
	DROP VIEW [SchemaName].[ViewName];
END