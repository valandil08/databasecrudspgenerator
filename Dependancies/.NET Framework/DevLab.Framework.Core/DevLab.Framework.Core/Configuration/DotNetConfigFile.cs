﻿using DevLab.Standard.Core.Configuration;
using System.Configuration;

namespace DevLab.Framework.Core.Configuration
{
    public class DotNetConfigFile : IConfig
    {
        public bool DoesAppSettingExist(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
            {
                return true;
            }

            return false;
        }

        public bool DoesConnectionStringExist(string key)
        {
            if (ConfigurationManager.ConnectionStrings[key] != null)
            {
                return true;
            }

            return false;
        }

        public string GetAppSetting(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        public string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }
    }
}
