﻿using DevLab.Standard.Core.Connections.SQL;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DevLab.Framework.Core.Connections.SQL
{
    public class SqlServerDb : SqlDb, ISqlDb
    {
        public SqlServerDb() : base(new SqlCommand()) { }

        public SqlServerDb(string connectionStringName) : base(new SqlCommand(), connectionStringName) { }

        protected override IDbConnection GenerateConnectionString(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        protected override DbDataAdapter GetDataAdapter()
        {
            return new SqlDataAdapter((SqlCommand)dbCommand);
        }

        public override bool IsConnectionStringValid(string connectionString)
        {
            return true;
        }

        protected override bool IsExceptionDeadlock(DbException ex)
        {
            return ((SqlException)ex).Number == 1205;
        }

        public override SqlDbEnum GetSqlDbType()
        {
            return SqlDbEnum.SqlServer;
        }
    }
}
