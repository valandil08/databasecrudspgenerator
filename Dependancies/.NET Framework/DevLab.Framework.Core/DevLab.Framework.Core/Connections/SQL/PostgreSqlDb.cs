﻿using System.Data;
using Npgsql;
using System.Data.Common;
using DevLab.Standard.Core.Connections.SQL;

namespace DevLab.Framework.Core.Connections.SQL
{
    public class PostgreSqlDb : SqlDb, ISqlDb
    {
        public PostgreSqlDb() : base(new NpgsqlCommand()) { }

        public PostgreSqlDb(string connectionStringName) : base(new NpgsqlCommand(), connectionStringName) { }

        protected override IDbConnection GenerateConnectionString(string connectionString)
        {
            return new NpgsqlConnection(connectionString);
        }

        protected override DbDataAdapter GetDataAdapter()
        {
            return new NpgsqlDataAdapter((NpgsqlCommand)dbCommand);
        }

        public override bool IsConnectionStringValid(string connectionString)
        {
            return true;
        }

        protected override bool IsExceptionDeadlock(DbException ex)
        {
            return ((NpgsqlException)ex).ErrorCode == 1205;
        }

        public override SqlDbEnum GetSqlDbType()
        {
            return SqlDbEnum.PostgreSQL;
        }
    }
}
