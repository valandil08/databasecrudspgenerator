﻿namespace System
{
    public static class StringExtensions
    {
        public static bool BeginsWith(this string text, string value, bool caseSensertive = false)
        {
            int valueLength = value.Length;

            if (valueLength > text.Length)
            {
                return false;
            }

            if (caseSensertive)
            {
                if (text.Substring(0, valueLength).Equals(value))
                {
                    return true;
                }
            }
            else
            {

                if (text.Substring(0, valueLength).ToLower().Equals(value.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool EndsWith(this string text, string value, bool caseSensertive = false)
        {
            int valueLength = value.Length;

            if (valueLength > text.Length)
            {
                return false;
            }

            if (caseSensertive)
            {
                if (text.Substring(text.Length - valueLength, valueLength).Equals(value))
                {
                    return true;
                }
            }
            else
            {

                if (text.Substring(text.Length - valueLength, valueLength).ToLower().Equals(value.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

        public static byte? ToByteNull(this string text)
        {
            if (byte.TryParse(text, out byte value))
            {
                return value;
            }

            return null;
        }

        public static sbyte? ToSByteNull(this string text)
        {
            if (sbyte.TryParse(text, out sbyte value))
            {
                return value;
            }

            return null;
        }

        public static short? ToShortNull(this string text)
        {
            if (short.TryParse(text, out short value))
            {
                return value;
            }

            return null;
        }

        public static ushort? ToUShortNull(this string text)
        {
            if (ushort.TryParse(text, out ushort value))
            {
                return value;
            }

            return null;
        }

        public static int? ToIntNull(this string text)
        {
            if (int.TryParse(text, out int value))
            {
                return value;
            }

            return null;
        }

        public static uint? ToUIntNull(this string text)
        {
            if (uint.TryParse(text, out uint value))
            {
                return value;
            }

            return null;
        }

        public static long? ToLongNull(this string text)
        {
            if (long.TryParse(text, out long value))
            {
                return value;
            }

            return null;
        }

        public static ulong? ToULongNull(this string text)
        {
            if (ulong.TryParse(text, out ulong value))
            {
                return value;
            }

            return null;
        }

        //public static float? ToFloatNull(this string text)
        //{
        //    if (float.TryParse(text, out float value))
        //    {
        //        return value;
        //    }

        //    return null;
        //}

        //public static double? ToDoubleNull(this string text)
        //{
        //    try
        //    {
        //        return Convert.ToDouble(text);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public static char? ToCharNull(this string text)
        {
            if (char.TryParse(text, out char value))
            {
                return value;
            }

            return null;
        }

        public static decimal? ToDecimalNull(this string text)
        {
            if (decimal.TryParse(text, out decimal value))
            {
                return value;
            }

            return null;
        }

        public static bool? ToBoolNull(this string text)
        {
            if (bool.TryParse(text, out bool value))
            {
                return value;
            }

            return null;
        }

        public static DateTime? ToDateTimeNull(this string text)
        {
            if (DateTime.TryParse(text, out DateTime value))
            {
                return value;
            }

            return null;
        }

        public static TimeSpan? ToTimeSpanNull(this string text)
        {
            if (TimeSpan.TryParse(text, out TimeSpan value))
            {
                return value;
            }

            return null;
        }
    }
}