﻿namespace System
{
    public static class DateTimeExtensions
    {
        public static bool IsWeekendDay(this DateTime dt)
        {
            return dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday;
        }

        public static DateTime GetDayOfWeek(this DateTime dt, DayOfWeek day)
        {
            int diff = (7 + (dt.DayOfWeek - day)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }


        public static bool IsDayOfWeek(this DateTime dt, params DayOfWeek[] days)
        {
            foreach (DayOfWeek day in days)
            {
                if (dt.DayOfWeek == day)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
