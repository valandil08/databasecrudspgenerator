﻿namespace System
{
    public static class BoolExtensions
    {
        public static char ToSqlBit(this bool value)
        {
            if (value)
            {
                return '1';
            }

            return '0';
        }

        public static char? ToSqlBit(this bool? value)
        {
            if (value == null)
            {
                return null;
            }

            if (value == true)
            {
                return '1';
            }

            return '0';
        }

    }
}
