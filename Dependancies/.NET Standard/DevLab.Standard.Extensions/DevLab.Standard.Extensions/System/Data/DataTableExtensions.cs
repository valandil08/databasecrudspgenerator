﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Data
{
    public static class DataTableExtensions
    {
        public static string ToCSV(this DataTable dt)
        {
            StringBuilder sb = new StringBuilder();

            string[] columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName).
                                              ToArray();
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                ToArray();
                sb.AppendLine(string.Join(",", fields));
            }

            return sb.ToString();
        }

        #region Row to Object Mapping Methods

        public static List<T> ToList<T>(this DataTable dt, Func<DataRow, T> selector) where T : new()
        {
            List<T> list = new List<T>();

            foreach (DataRow row in dt.Rows)
            {
                T value = selector.Invoke(row);

                list.Add(value);
            }

            return list;
        }

        public static T ConvertRowToObject<T>(this DataTable dt, int rowNum, Func<DataRow, T> selector) where T : new()
        {
            if (dt.Rows.Count <= rowNum)
            {
                return default(T);
            }

            List<T> list = dt.Rows[rowNum].Table.ToList(selector);

            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return default(T);
            }
        }

        public static T ConvertFirstRowToObject<T>(this DataTable dt, Func<DataRow, T> selector) where T : new()
        {
            return ConvertRowToObject(dt, 0, selector);
        }

        #endregion

    }
}