﻿using System.Diagnostics;
using System.Reflection;

namespace System
{
    public static class ExceptionExtensions
    {
        public static int GetLineNumber(this Exception ex)
        {
            StackTrace st = new StackTrace(ex, true);
            StackFrame frame = st.GetFrame(0);

            return frame.GetFileLineNumber();
        }

        public static MethodInfo GetMethodInfo(this Exception ex)
        {
            StackTrace st = new StackTrace(ex, true);
            StackFrame frame = st.GetFrame(0);
            MethodBase methodData = frame.GetMethod();

            return (MethodInfo)methodData;
        }
    }
}
