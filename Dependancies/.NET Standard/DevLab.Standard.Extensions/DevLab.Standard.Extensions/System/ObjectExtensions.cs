﻿namespace System
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// converts the object to either a string using the ToString() method or a null or empty string 
        /// depending on what replaceNullWithEmptyString is set to
        /// </summary>
        public static string ToStringNull(this object obj, bool replaceNullWithEmptyString = false)
        {
            if (obj == null)
            {
                if (replaceNullWithEmptyString)
                {
                    return string.Empty;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return obj.ToString();
            }
        }
    }
}
