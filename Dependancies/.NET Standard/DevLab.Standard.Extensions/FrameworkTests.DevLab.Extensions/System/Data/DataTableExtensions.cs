﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Data;

namespace System.Data
{
    [TestClass()]
    public class DataTableExtensionsTests
    {
        // use same data set for all tests
        DataTable dt = GenerateDataTable();

        [TestMethod()]
        public void ConvertFirstRowToObjectTest()
        {
            TestObject model = dt.ConvertFirstRowToObject(
                 row => new TestObject
                 {
                     id = row.Field<int>("id"),
                     name = row.Field<string>("name")
                 }
            );

            if (model == null)
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void ConvertRowToObjectTest()
        {
            TestObject model = dt.ConvertRowToObject(0,
                 row => new TestObject
                 {
                     id = row.Field<int>("id"),
                     name = row.Field<string>("name")
                 }
            );

            if (model == null)
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void ToListTest()
        {
            List<TestObject> list = dt.ToList(
                 row => new TestObject
                 {
                     id = row.Field<int>("id"),
                     name = row.Field<string>("name")
                 }
            );

            if (list.Count != dt.Rows.Count)
            {
                Assert.Fail();
            }

            for (int i = 0; i < list.Count; i++)
            {
                Assert.AreEqual("name" + i, list[i].name);
            }
        }
                     
        #region Support Methods and Classes

        private static DataTable GenerateDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(int));
            dt.Columns.Add("name", typeof(String));
            for (int i = 0; i < 5; i++)
            {
                dt.Rows.Add(new object[] { i, "name" + i });
            }

            return dt;
        }

        private class TestObject
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        #endregion
    }
}