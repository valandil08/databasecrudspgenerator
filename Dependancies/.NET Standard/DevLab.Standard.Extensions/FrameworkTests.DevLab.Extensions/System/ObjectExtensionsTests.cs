﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace System
{
    [TestClass()]
    public class ObjectExtensionsTests
    {
        [TestMethod()]
        public void ToStringNullTest()
        {
            string test = null;

            if (test.ToStringNull() != null)
            {
                Assert.Fail();
            }

            test = "";

            if (test.ToStringNull() == null)
            {
                Assert.Fail();
            }

            test = "test";

            if (test.ToStringNull() == null)
            {
                Assert.Fail();
            }
        }
    }
}