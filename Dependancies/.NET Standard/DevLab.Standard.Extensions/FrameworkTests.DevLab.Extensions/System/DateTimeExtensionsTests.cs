﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace System
{
    [TestClass()]
    public class DateTimeExtensionsTests
    {
        // Monday 1st january 2018
        DateTime date = new DateTime(2018, 1, 1);

        [TestMethod()]
        public void IsWeekendDayTest()
        {
            for (int i = 0; i < 7; i++)
            {
                DateTime day = date.AddDays(i);

                if (day.IsWeekendDay() && day.Day <= 5)
                {
                    Assert.Fail();
                }

                if (day.IsWeekendDay() == false && day.Day > 5)
                {
                    Assert.Fail();
                }
            }
        }

        [TestMethod()]
        public void GetDayOfWeekTest()
        {
            for (int i = 0; i < 7; i++)
            {
                DateTime day = date.AddDays(i - 1);

                Assert.AreEqual(day.DayOfWeek, (DayOfWeek)i);
            }
        }

        [TestMethod()]
        public void IsDayOfWeekTest()
        {
            if (date.IsDayOfWeek(
                DayOfWeek.Monday,
                DayOfWeek.Tuesday,
                DayOfWeek.Wednesday,
                DayOfWeek.Thursday,
                DayOfWeek.Friday,
                DayOfWeek.Saturday,
                DayOfWeek.Sunday
               ) == false)
            {
                Assert.Fail();
            }


            for (int i = 0; i < 7; i++)
            {
                DateTime day = date.GetDayOfWeek((DayOfWeek)i);

                Assert.AreEqual(day.DayOfWeek, (DayOfWeek)i);
            }
        }
    }
}