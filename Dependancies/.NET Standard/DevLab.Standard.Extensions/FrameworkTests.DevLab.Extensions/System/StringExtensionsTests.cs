﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace System
{
    [TestClass()]
    public class StringExtensionsTests
    {
        [TestMethod()]
        public void BeginsWith_CheckValidCaseSensetiveInput_Pass()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsTrue(value.BeginsWith(value.Substring(0, 3), true));
            }
        }

        [TestMethod()]
        public void BeginsWith_CheckInvalidCaseSensetiveInput_Fail()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsFalse(value.BeginsWith("tess", true));
            }
        }

        [TestMethod()]
        public void BeginsWith_CheckValidNotCaseSensetiveInput_Pass()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsTrue(value.BeginsWith("tes", false));
            }
        }

        [TestMethod()]
        public void BeginsWith_CheckInvalidNotCaseSensetiveInput_PFail()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsFalse(value.BeginsWith("tess", false));
            }
        }


        [TestMethod()]
        public void EndsWith_CheckValidCaseSensetiveInput_Pass()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsTrue(value.EndsWith(value.Substring(4,3), true));
            }
        }

        [TestMethod()]
        public void EndsWith_CheckInvalidCaseSensetiveInput_Fail()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsFalse(value.EndsWith("ingg", true));
            }
        }

        [TestMethod()]
        public void EndsWith_CheckValidNotCaseSensetiveInput_Pass()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsTrue(value.EndsWith("ing", false));
            }
        }

        [TestMethod()]
        public void EndsWith_CheckInvalidNotCaseSensetiveInput_PFail()
        {
            foreach (string value in GetAllCaseCombinationsOfWord("testing"))
            {
                Assert.IsFalse(value.EndsWith("ingg", false));
            }
        }

        [TestMethod()]
        public void ToByteNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((byte.MinValue + "").ToByteNull(), byte.MinValue);
            Assert.AreEqual((byte.MaxValue + "").ToByteNull(), byte.MaxValue);
        }

        [TestMethod()]
        public void ToByteNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("333333".ToByteNull(), null);
        }

        [TestMethod()]
        public void ToSByteNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((sbyte.MinValue + "").ToSByteNull(), sbyte.MinValue);
            Assert.AreEqual((sbyte.MaxValue + "").ToSByteNull(), sbyte.MaxValue);
        }

        [TestMethod()]
        public void ToSByteNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("333333".ToSByteNull(), null);
        }


        [TestMethod()]
        public void ToShortNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((short.MinValue + "").ToShortNull(), short.MinValue);
            Assert.AreEqual((short.MaxValue + "").ToShortNull(), short.MaxValue);
        }

        [TestMethod()]
        public void ToShortNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToShortNull(), null);
        }

        [TestMethod()]
        public void ToUShortNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((ushort.MinValue + "").ToUShortNull(), ushort.MinValue);
            Assert.AreEqual((ushort.MaxValue + "").ToUShortNull(), ushort.MaxValue);
        }

        [TestMethod()]
        public void ToUShortNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToUShortNull(), null);
        }

        [TestMethod()]
        public void ToIntNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((int.MinValue + "").ToIntNull(), int.MinValue);
            Assert.AreEqual((int.MaxValue + "").ToIntNull(), int.MaxValue);
        }

        [TestMethod()]
        public void ToIntNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToIntNull(), null);
        }
        
        [TestMethod()]
        public void ToUIntNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((uint.MinValue + "").ToUIntNull(), uint.MinValue);
            Assert.AreEqual((uint.MaxValue + "").ToUIntNull(), uint.MaxValue);
        }

        [TestMethod()]
        public void ToUIntNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToIntNull(), null);
        }

        [TestMethod()]
        public void ToLongNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((long.MinValue + "").ToLongNull(), long.MinValue);
            Assert.AreEqual((long.MaxValue + "").ToLongNull(), long.MaxValue);
        }

        [TestMethod()]
        public void ToLongNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToIntNull(), null);
        }

        [TestMethod()]
        public void ToULongNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((ulong.MinValue + "").ToULongNull(), ulong.MinValue);
            Assert.AreEqual((ulong.MaxValue + "").ToULongNull(), ulong.MaxValue);
        }

        [TestMethod()]
        public void ToULongNull_InvalidNumber_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToIntNull(), null);
        }

        [TestMethod()]
        public void ToCharNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((char.MinValue + "").ToCharNull(), char.MinValue);
            Assert.AreEqual((char.MaxValue + "").ToCharNull(), char.MaxValue);
        }

        [TestMethod()]
        public void ToCharNull_InvalidInput_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToCharNull(), null);
        }

        [TestMethod()]
        public void ToDecimalNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((decimal.MinValue + "").ToDecimalNull(), decimal.MinValue);
            Assert.AreEqual((decimal.MaxValue + "").ToDecimalNull(), decimal.MaxValue);
        }

        [TestMethod()]
        public void ToDecimalNull_InvalidInput_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToDecimalNull(), null);
        }

        [TestMethod()]
        public void ToBoolNull_ParseValidTrueText_Pass()
        {
            List<string> values = GetAllCaseCombinationsOfWord("true");

            foreach (string value in values)
            {
                Assert.AreEqual(value.ToBoolNull(), true);
            }

        }

        [TestMethod()]
        public void ToBoolNull_ParseValidFalseText_Pass()
        {
            List<string> values = GetAllCaseCombinationsOfWord("false");

            foreach (string value in values)
            {
                Assert.AreEqual(value.ToBoolNull(), false);
            }

        }

        [TestMethod()]
        public void ToBoolNull_ParseInvalidText_Fail()
        {
            List<string> values = GetAllCaseCombinationsOfWord("test");

            foreach (string value in values)
            {
                Assert.AreEqual(value.ToBoolNull(), null);
            }

        }

        [TestMethod()]
        public void ToDateTimeNull_MinAndMaxValues_Pass()
        {
            DateTime min = (DateTime.MinValue.ToString() + "." + DateTime.MinValue.Millisecond).ToDateTimeNull().Value;

            Assert.AreEqual(min.Year, DateTime.MinValue.Year);
            Assert.AreEqual(min.Month, DateTime.MinValue.Month);
            Assert.AreEqual(min.Day, DateTime.MinValue.Day);
            Assert.AreEqual(min.Hour, DateTime.MinValue.Hour);
            Assert.AreEqual(min.Minute, DateTime.MinValue.Minute);
            Assert.AreEqual(min.Second, DateTime.MinValue.Second);
            Assert.AreEqual(min.Millisecond, DateTime.MinValue.Millisecond);
            Assert.AreEqual(min.DayOfWeek, DateTime.MinValue.DayOfWeek);
            Assert.AreEqual(min.DayOfYear, DateTime.MinValue.DayOfYear);

            string maxDateTime = DateTime.MaxValue.ToString() + "." + DateTime.MaxValue.Millisecond;
            DateTime max = (maxDateTime + "").ToDateTimeNull().Value;

            Assert.AreEqual(max.Year, DateTime.MaxValue.Year);
            Assert.AreEqual(max.Month, DateTime.MaxValue.Month);
            Assert.AreEqual(max.Day, DateTime.MaxValue.Day);
            Assert.AreEqual(max.Hour, DateTime.MaxValue.Hour);
            Assert.AreEqual(max.Minute, DateTime.MaxValue.Minute);
            Assert.AreEqual(max.Second, DateTime.MaxValue.Second);
            Assert.AreEqual(max.Millisecond, DateTime.MaxValue.Millisecond);
            Assert.AreEqual(max.DayOfWeek, DateTime.MaxValue.DayOfWeek);
            Assert.AreEqual(max.DayOfYear, DateTime.MaxValue.DayOfYear);

        }

        [TestMethod()]
        public void ToDateTimeNull_InvalidInput_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToDateTimeNull(), null);
        }

        [TestMethod()]
        public void ToTimeSpanNull_MinAndMaxValues_Pass()
        {
            Assert.AreEqual((TimeSpan.MinValue + "").ToTimeSpanNull(), TimeSpan.MinValue);
            Assert.AreEqual((TimeSpan.MaxValue + "").ToTimeSpanNull(), TimeSpan.MaxValue);
        }

        [TestMethod()]
        public void ToTimeSpanNull_InvalidInput_Fail()
        {
            Assert.AreEqual("3333333333333333333333333333333333333333333333333333333333333333".ToTimeSpanNull(), null);
        }

        private List<string> GetAllCaseCombinationsOfWord(string word)
        {
            return GetAllCaseCombinationsOfWord(word.ToCharArray());
        }

        private List<string> GetAllCaseCombinationsOfWord(char[] word, string text = "", int level = 0)
        {
            List<string> list = new List<string>();

            if (level < word.Length)
            {
                list.AddRange(GetAllCaseCombinationsOfWord(word, text + char.ToLower(word[level]), level + 1));
                list.AddRange(GetAllCaseCombinationsOfWord(word, text + char.ToUpper(word[level]), level + 1));
            }
            else
            {
                list.Add(text);
            }

            return list;
        }
    }
}