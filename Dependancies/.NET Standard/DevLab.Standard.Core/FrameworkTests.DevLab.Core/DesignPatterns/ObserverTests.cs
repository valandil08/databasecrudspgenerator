﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace DevLab.Standard.Core.DesignPatterns.Tests
{
    [TestClass()]
    public class ObserverTests
    {
        [TestMethod()]
        public void Observer_CreateSubscribeAndNotifty_DataPassedThroughToSubscriber()
        {
            object obj = "";

            Observer<string> observer = new Observer<string>();


            string text = "beta";

            observer.Subscribe(obj, (value) =>
            {
                text = value;
            });

            observer.Notify("alpha");

            Assert.AreEqual(text, "alpha");
        }
    }
}