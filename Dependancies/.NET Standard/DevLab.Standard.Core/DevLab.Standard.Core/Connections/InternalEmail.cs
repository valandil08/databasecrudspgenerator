﻿using System;
using System.Net.Mail;
using System.Text;

namespace DevLab.Core.Connections
{
    public class InternalEmail
    {
        private static SmtpDeliveryMethod smptDeliveryMethod = SmtpDeliveryMethod.Network;
        private static bool useDefaultCredentials = true;
        private static bool enableSSL = false;
        private static int port = 25;

        #region Plain Text Emails

        /// <summary>
        /// Sends a plain text email.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        public static bool SendPlainTextEmail(string from, string to, string subject, string body)
        {
            return SendPlainTextEmail(from, to, subject, body, new string[] { });
        }

        /// <summary>
        /// Sends a plain text email with a attachment
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachment">The attachment.</param>
        public static bool SendPlainTextEmail(string from, string to, string subject, string body, string attachment)
        {
            return SendPlainTextEmail(from, to, subject, body, new string[] { attachment });
        }

        /// <summary>
        /// Sends a plain text email with multiple attachments
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachments">The attachments.</param>
        public static bool SendPlainTextEmail(string from, string to, string subject, string body, string[] attachments)
        {
            return SendEmail(from, to, subject, body, attachments, false);
        }

        /// <summary>
        /// Sends a plain text email with a attachment
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachment">The attachment.</param>
        public static bool SendPlainTextEmail(string from, string to, string subject, string body, Attachment attachment)
        {
            return SendPlainTextEmail(from, to, subject, body, new Attachment[] { attachment });
        }

        /// <summary>
        /// Sends a plain text email with multiple attachments
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachments">The attachments.</param>
        public static bool SendPlainTextEmail(string from, string to, string subject, string body, Attachment[] attachments)
        {
            return SendEmail(from, to, subject, body, attachments, false);
        }

        #endregion

        #region HTML Emails

        /// <summary>
        /// Sends a HTML email.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        public static bool SendHtmlEmail(string from, string to, string subject, string body)
        {
            return SendHtmlEmail(from, to, subject, body, new string[] { });
        }

        /// <summary>
        /// Sends a HTML email with a attachment
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachment">The attachment.</param>
        public static bool SendHtmlEmail(string from, string to, string subject, string body, string attachment)
        {
            return SendHtmlEmail(from, to, subject, body, new string[] { attachment });
        }

        /// <summary>
        /// Sends a HTML email with attachments
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachments">The attachments.</param>
        public static bool SendHtmlEmail(string from, string to, string subject, string body, string[] attachments)
        {
            return SendEmail(from, to, subject, body, attachments, true);
        }

        /// <summary>
        /// Sends a HTML email with a attachment
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachment">The attachment.</param>
        public static bool SendHtmlEmail(string from, string to, string subject, string body, Attachment attachment)
        {
            return SendHtmlEmail(from, to, subject, body, new Attachment[] { attachment });
        }

        /// <summary>
        /// Sends a HTML email with attachments
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachments">The attachments.</param>
        public static bool SendHtmlEmail(string from, string to, string subject, string body, Attachment[] attachments)
        {
            return SendEmail(from, to, subject, body, attachments, true);
        }

        #endregion


        /// <summary>
        /// Sends a email according to parameters set
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachments">The attachments.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        private static bool SendEmail(string from, string to, string subject, string body, string[] attachments, bool isHtml)
        {
            try
            {
                Attachment[] arr = new Attachment[attachments.Length];

                if (attachments != null)
                {
                    for (int i = 0; i < attachments.Length; i++)
                    {
                        arr[i] = new Attachment(attachments[i]);
                    }
                }

                return SendEmail(from, to, subject, body, attachments, isHtml);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static bool SendEmail(string from, string to, string subject, string body, Attachment[] attachments, bool isHtml)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(from);

                    foreach (string email in to.Split(','))
                    {
                        mail.To.Add(email);
                    }

                    mail.Subject = subject;
                    mail.Body = body;
                    mail.BodyEncoding = Encoding.ASCII;
                    mail.IsBodyHtml = isHtml;

                    if (attachments != null)
                    {
                        foreach (Attachment attachment in attachments)
                        {
                            mail.Attachments.Add(attachment);
                        }
                    }

                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.Host = AppConfig.GetAppSetting("EmailServerHostIp");
                        smtp.Port = port;
                        smtp.DeliveryMethod = smptDeliveryMethod;
                        smtp.UseDefaultCredentials = useDefaultCredentials;
                        smtp.EnableSsl = enableSSL;

                        try
                        {
                            smtp.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Exception caught in SendEmail(): {0}" + ex.Message);
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
