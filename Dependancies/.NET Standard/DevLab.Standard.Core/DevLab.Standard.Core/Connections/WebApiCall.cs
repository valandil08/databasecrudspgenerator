﻿//using System;
//using System.Linq;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Web;

//namespace DevLab.Core.Connections
//{
//    public class WebApiCall
//    {
//        public static bool RunDeleteRequest(string baseAddress, string apiMethod, int id)
//        {
//            try
//            {
//                using (HttpClient client = GetHttpClient(baseAddress))
//                {
//                    HttpResponseMessage response = client.DeleteAsync(apiMethod + "/" + id).Result;

//                    return response.IsSuccessStatusCode;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = ex.Message;
//                throw ex;
//            }
//        }

//        public static object RunGetRequest(string baseAddress, string apiMethod, object obj)
//        {
//            try
//            {
//                string queryString = ConvertObjectToQueryString(obj);

//                using (HttpClient client = GetHttpClient(baseAddress))
//                {
//                    HttpResponseMessage response = client.GetAsync(apiMethod + "/" + queryString).Result;

//                    if (response.IsSuccessStatusCode)
//                    {
//                        return response.Content.ReadAsAsync<object>().Result;
//                    }
//                    else
//                    {
//                        throw new Exception("Error running WebAPI Get Request");
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = ex.Message;
//                throw ex;
//            }
//        }

//        public static T RunGetRequest<T>(string baseAddress, string apiMethod, object obj)
//        {
//            try
//            {
//                string queryString = ConvertObjectToQueryString(obj);

//                using (HttpClient client = GetHttpClient(baseAddress))
//                {
//                    HttpResponseMessage response = client.GetAsync(apiMethod + "/" + queryString).Result;

//                    if (response.IsSuccessStatusCode)
//                    {
//                        return response.Content.ReadAsAsync<T>().Result;
//                    }
//                    else
//                    {
//                        throw new Exception("Error running WebAPI Get Request");
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = ex.Message;
//                throw ex;
//            }
//        }

//        public static bool RunPostRequest<T>(string baseAddress, string apiMethod, T value)
//        {
//            try
//            {
//                using (HttpClient client = GetHttpClient(baseAddress))
//                {
//                    HttpResponseMessage response = client.PostAsJsonAsync<T>(apiMethod, value).Result;

//                    return response.IsSuccessStatusCode;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = ex.Message;
//                throw ex;
//            }
//        }

//        public static bool RunPutRequest<T>(string baseAddress, string apiMethod, T value)
//        {
//            try
//            {
//                using (HttpClient client = GetHttpClient(baseAddress))
//                {
//                    HttpResponseMessage response = client.PutAsJsonAsync<T>(apiMethod, value).Result;

//                    return response.IsSuccessStatusCode;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = ex.Message;
//                throw ex;
//            }
//        }

//        private static string ConvertObjectToQueryString(object obj)
//        {
//            var properties = from p in obj.GetType().GetProperties()
//                             where p.GetValue(obj, null) != null
//                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

//            string value = String.Join("&", properties.ToArray());


//            if (value.Equals(""))
//            {
//                value = "id=" + obj.ToString();
//            }

//            if (!value.Equals(""))
//            {
//                value = "?" + value;
//            }

//            return value;
//        }

//        private static HttpClient GetHttpClient(string baseAddress)
//        {
//            HttpClient client = new HttpClient();

//            client.BaseAddress = new Uri(baseAddress);
//            client.DefaultRequestHeaders.Accept.Clear();
//            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

//            return client;
//        }
//    }
//}