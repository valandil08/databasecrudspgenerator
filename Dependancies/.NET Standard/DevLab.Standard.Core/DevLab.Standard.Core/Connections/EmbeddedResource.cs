﻿using System;
using System.IO;
using System.Reflection;

namespace DevLab.Standard.Core.Connections
{
    public class EmbeddedResource
    {
        public static string GetResourceSqlFile(string dir, string fileName)
        {
            string baseUrl = AppDomain.CurrentDomain.BaseDirectory;
            string resourceName = baseUrl + "Resources\\" + fileName + ".sql";

            string sql = File.ReadAllText(resourceName);

            if (sql == null)
            {
                return "";
            }

            return sql;
        }
    }
}
