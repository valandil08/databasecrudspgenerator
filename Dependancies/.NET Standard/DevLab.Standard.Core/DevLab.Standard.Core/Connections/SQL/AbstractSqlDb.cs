﻿using DevLab.Standard.Core.Connections.SQL;
using System;
using System.Data;
using System.Data.Common;

namespace DevLab.Standard.Core.Connections.SQL
{
    public abstract class SqlDb : ISqlDb
    {
        private delegate object SqlRequestDelegate();

        protected IDbCommand dbCommand;
        private int maxNumReattempts = 3;

        public SqlDb(IDbCommand dbCommand, string connectionStringName = null)
        {
            this.dbCommand = dbCommand;

            if (connectionStringName != null)
            {
                string connectionString = GetConnectionString(connectionStringName);

                if (IsConnectionStringValid(connectionString))
                {
                    dbCommand.Connection = GenerateConnectionString(connectionString);
                }
                else
                {
                    throw new Exception("(SqlDb) Invalid connection string: " + connectionString);
                }
            }
        }

        public void AddParameter(string name, string value)
        {
            IDbDataParameter parameter = dbCommand.CreateParameter();
            parameter.ParameterName = name;
            parameter.Value = value;

            dbCommand.Parameters.Add(parameter);
        }

        public void AddParameter<T>(string name, T value)
        {
            IDbDataParameter parameter = dbCommand.CreateParameter();
            parameter.ParameterName = name;
            parameter.Value = value;

            dbCommand.Parameters.Add(parameter);
        }

        public DataSet ExecuteDataSetRequest()
        {
            object obj = null;

            obj = ExecuteSqlRequest(delegate ()
            {
                DataSet dataSet = new DataSet();
                using (DbDataAdapter da = GetDataAdapter())
                {
                    da.Fill(dataSet);
                    da.Dispose();
                }
                return dataSet;
            });

            if (obj == null)
            {
                return null;
            }
            else
            {
                return (DataSet)obj;
            }
        }

        public DataTable ExecuteDataTableRequest()
        {
            DataSet ds = ExecuteDataSetRequest();

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
            }

            return null;
        }

        public object ExecuteScalarRequest()
        {
            return ExecuteSqlRequest(delegate ()
            {
                return dbCommand.ExecuteScalar();
            });
        }

        public void ExecuteRequest()
        {
            ExecuteSqlRequest(delegate ()
            {
                dbCommand.ExecuteNonQuery();
                return null;
            });
        }

        // Common code shared among all Execute methods
        private object ExecuteSqlRequest(SqlRequestDelegate request)
        {
            object obj = null;

            OpenConnection(dbCommand.Connection);

            int attemptCounter = 0;

            while (attemptCounter < maxNumReattempts)
            {
                try
                {
                    obj = request();

                    break;
                }
                catch (DbException sqlEx)
                {
                    if (IsExceptionDeadlock(sqlEx) && attemptCounter < maxNumReattempts)
                    {
                        attemptCounter++;
                    }
                    else
                    {
                        throw new Exception("(SqlDb) ProcessRequest: (" + dbCommand.CommandText + ") " + sqlEx.Message, sqlEx);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("(SqlDb) ProcessRequest: (" + dbCommand.CommandText + ")" + ex.Message, ex);
                }
            }

            dbCommand.Connection.Close();

            return obj;
        }



        public void SetConnectionString(string connectionString)
        {
            if (IsConnectionStringValid(connectionString))
            {
                dbCommand.Connection = GenerateConnectionString(connectionString);
            }
            else
            {
                throw new Exception("(SqlDb) Invalid connection string: " + connectionString);
            }
        }

        public void SetMaxReattempts(int numAttempts)
        {
            maxNumReattempts = numAttempts;
        }

        public void SetSql(string sql)
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = sql;
        }

        public void SetStoredProcedure(string storedProcedureName)
        {
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.CommandText = storedProcedureName;
        }

        public void SetTimeout(int seconds)
        {
            dbCommand.CommandTimeout = seconds;
        }


        private void OpenConnection(IDbConnection connection)
        {
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message; // for debugging
                connection.Close();
                throw new Exception("(SqlDb) Error opening connection : " + connection.ConnectionString + "\n\r" + errorMessage, ex);
            }
        }

        private string GetConnectionString(string connectionStringName)
        {
            return AppConfig.GetConnectionString(connectionStringName);
        }


        protected abstract IDbConnection GenerateConnectionString(string connectionString);

        protected abstract DbDataAdapter GetDataAdapter();

        public abstract bool IsConnectionStringValid(string connectionString);

        protected abstract bool IsExceptionDeadlock(DbException ex);

        public abstract SqlDbEnum GetSqlDbType();
    }
}
