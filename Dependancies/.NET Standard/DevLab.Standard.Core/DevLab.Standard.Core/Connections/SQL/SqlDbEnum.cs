﻿namespace DevLab.Standard.Core.Connections.SQL
{
    public enum SqlDbEnum
    {
        SqlServer,
        PostgreSQL
    }
}
