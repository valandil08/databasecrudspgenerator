﻿using System.Data;

namespace DevLab.Standard.Core.Connections.SQL
{
    public interface ISqlDb
    {
        void SetSql(string sql);
        void SetStoredProcedure(string storedProcedureName);

        void AddParameter(string name, string value);
        void AddParameter<T>(string name, T value);

        void SetMaxReattempts(int numAttempts);
        void SetTimeout(int seconds);
        void SetConnectionString(string connectionString);

        object ExecuteScalarRequest();
        DataTable ExecuteDataTableRequest();
        DataSet ExecuteDataSetRequest();
        void ExecuteRequest();

        SqlDbEnum GetSqlDbType();
    }
}
