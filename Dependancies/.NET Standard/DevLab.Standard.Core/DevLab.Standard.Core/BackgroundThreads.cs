﻿using System.Collections.Generic;
using System.Threading;

namespace DevLab.Standard.Core
{
    public class BackgroundThreads
    {
        private static List<ThreadContainer> threads = new List<ThreadContainer>();

        public static bool AddThread(string name, Thread thread)
        {
            if (DoesThreadExist(name))
            {
                return false;
            }

            ThreadContainer container = new ThreadContainer(name, thread);
            threads.Add(container);

            return true;
        }

        public static bool StartThread(string name)
        {
            foreach (ThreadContainer thread in threads)
            {
                if (thread.GetThreadName().Equals(name))
                {
                    thread.StartThread();
                    return true;
                }
            }

            return false;
        }

        public static bool DoesThreadExist(string name)
        {
            foreach (ThreadContainer thread in threads)
            {
                if (thread.name.Equals(name))
                {
                    return true;
                }
            }

            return false;
        }

        private class ThreadContainer
        {
            public string name;
            public Thread thread;

            public ThreadContainer(string name, Thread thread)
            {
                this.name = name;
                this.thread = thread;
            }

            public void StartThread()
            {
                thread.IsBackground = true;
                thread.Start();
            }

            public bool IsThreadRunning()
            {
                return thread.ThreadState == ThreadState.Background;
            }

            public string GetThreadName()
            {
                return name;
            }
        }
    }
}
