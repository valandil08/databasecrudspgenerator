﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DevLab.Standard.Core.DesignPatterns
{
    public class Observer<T>
    {
        private ObjectIDGenerator ObjectIDGen = new ObjectIDGenerator();
        private Dictionary<long, Action<T>> subscriptions = new Dictionary<long, Action<T>>();

        public bool Subscribe(object client, Action<T> func)
        {
            try
            {
                long clientId = GetClientId(client);

                if (subscriptions.ContainsKey(clientId))
                {
                    return false;
                }

                subscriptions.Add(clientId, func);

                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                return false;
            }
        }

        public bool Unsubscribe(object client)
        {
            try
            {
                long clientId = GetClientId(client);


                if (subscriptions.ContainsKey(clientId) == false)
                {
                    return false;
                }

                subscriptions.Remove(clientId);

                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                return false;
            }
        }

        public bool Notify(T data)
        {
            try
            {
                foreach (KeyValuePair<long, Action<T>> sub in subscriptions)
                {
                    sub.Value.Invoke(data);
                }

                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                return false;
            }
        }

        private long GetClientId(object obj)
        {
            bool first = false;
            return ObjectIDGen.GetId(obj, out first);
        }

    }
}
