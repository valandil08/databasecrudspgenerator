﻿using System;
using System.Threading;

namespace DevLab.Standard.Core
{
    public class DataSync<T>
    {
        private T Value;
        private DateTime LastUpdated;

        private Func<T> UpdateCode;
        private int DelayBetweenUpdatesInMilliSeconds;

        private Thread UpdateThread;

        public DataSync(int delayBetweenUpdatesInMilliSeconds, Func<T> updateCode)
        {
            DelayBetweenUpdatesInMilliSeconds = delayBetweenUpdatesInMilliSeconds;
            UpdateCode = updateCode;

            LastUpdated = DateTime.Now.AddDays(-1);

            UpdateThread = new Thread(UpdateThreadCode);
            UpdateThread.Start();
        }

        private void UpdateThreadCode()
        {
            while (!Environment.HasShutdownStarted)
            {
                while (DateTime.Now < LastUpdated.AddMilliseconds(DelayBetweenUpdatesInMilliSeconds))
                {
                    Thread.Sleep(100);
                }

                try
                {
                    Value = UpdateCode.Invoke();
                    LastUpdated = DateTime.Now;
                }
                catch (Exception ex)
                {
                    string errorMessage = ex.Message;
                }
            }
        }

        public bool HasUpdated(DateTime lastCientUpdate)
        {
            return LastUpdated.Ticks > lastCientUpdate.Ticks;
        }

        public DateTime GetLastUpdated()
        {
            return LastUpdated;
        }

        public T GetValue()
        {
            return Value;
        }
    }
}