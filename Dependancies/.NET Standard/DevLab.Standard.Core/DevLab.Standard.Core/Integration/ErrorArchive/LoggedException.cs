﻿using System;

namespace DevLab.Standard.Core.Integration.ErrorArchive
{
    public class LoggedException : Exception
    {
        public Exception Exception { get; }

        public LoggedException(Exception exception)
        {
            if (exception.GetType().ToString().Equals("LoggedException"))
            {
                this.Exception = ((LoggedException)exception).Exception;
            }
            else
            {
                this.Exception = exception;
            }
        }
    }
}
