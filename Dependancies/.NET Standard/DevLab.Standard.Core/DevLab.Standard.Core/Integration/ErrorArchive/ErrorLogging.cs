﻿using DevLab.Standard.Core.Connections.SQL;
using System;
using System.Diagnostics;
using System.Reflection;

namespace DevLab.Standard.Core.Integration.ErrorArchive
{
    public class ErrorLogging
    {
        private static ISqlDb SqlDatabase = null;

        public static void SetDatabaseConnection(ISqlDb sqlDatabase)
        {
            SqlDatabase = sqlDatabase;
        }

        public static Exception LogMethodWebError(Exception ex, string activeUser = null, string url = null, string queryString = null, string session = null, int? outerErrorId = null)
        {
            if (SqlDatabase == null)
            {
                return ex;
            }

            if (ex.GetType().ToString().Equals("LoggedException"))
            {
                return ex;
            }

            int methodDataId = GetMethodDataId(ex);
            int webDataId = GetWebDataId(url, queryString, session);

            int errorId = LogError(ex, activeUser, methodDataId, webDataId, outerErrorId);

            if (ex.InnerException != null)
            {
                LogMethodWebError(ex.InnerException, activeUser, url, queryString, session, errorId);
            }

            return new LoggedException(ex);

        }

        public static Exception LogMethodError(Exception ex, string activeUser = null, int? outerErrorId = null)
        {
            if (SqlDatabase == null)
            {
                return ex;
            }

            if (ex.GetType().ToString().Equals("LoggedException"))
            {
                return ex;
            }

            int methodDataId = GetMethodDataId(ex);

            int errorId = LogError(ex, activeUser, methodDataId, -1, outerErrorId);

            if (ex.InnerException != null)
            {
                LogMethodError(ex.InnerException, activeUser, errorId);
            }

            return new LoggedException(ex);
        }

        private static int LogError(Exception ex, string activeUser = null, int methodDataId = -1, int webDataId = -1, int? outerErrorId = null)
        {
            string system = AppConfig.GetAppSetting("System");

            SqlDatabase.SetStoredProcedure("[sqlDatabaseo].[usp_LogError]");
            SqlDatabase.AddParameter<string>("System", system);
            SqlDatabase.AddParameter<string>("Technology", "C#");
            SqlDatabase.AddParameter<string>("ErrorType", GetErrorType(ex));
            SqlDatabase.AddParameter<string>("ObjectThrownIn", GetObjectThrownIn(ex));
            SqlDatabase.AddParameter<string>("ErrorMessage", ex.Message);
            SqlDatabase.AddParameter<string>("MachineName", Environment.MachineName);
            SqlDatabase.AddParameter<string>("ActiveUser", activeUser);

            if (outerErrorId != null)
            {
                SqlDatabase.AddParameter("OuterErrorId", outerErrorId);
            }

            if (methodDataId != -1)
            {
                SqlDatabase.AddParameter("MethodDataId", methodDataId);
            }

            if (webDataId != -1)
            {
                SqlDatabase.AddParameter("WebDataId", webDataId);
            }

            return (int)SqlDatabase.ExecuteScalarRequest();
        }

        private static int GetMethodDataId(Exception ex)
        {
            SqlDatabase.SetStoredProcedure("[sqlDatabaseo].[usp_LogError_MethodData]");
            SqlDatabase.AddParameter<string>("Class", GetClassName(ex));
            SqlDatabase.AddParameter<string>("Package", GetPackageName(ex));
            SqlDatabase.AddParameter<string>("MethodParameters", GetMethodParameters(ex));
            SqlDatabase.AddParameter<string>("StackTrace", ex.StackTrace);

            return (int)SqlDatabase.ExecuteScalarRequest();
        }

        private static int GetWebDataId(string url = null, string queryString = null, string session = null)
        {
            SqlDatabase.SetStoredProcedure("[sqlDatabaseo].[usp_LogError_WebData]");
            SqlDatabase.AddParameter<string>("URL", url);
            SqlDatabase.AddParameter<string>("QueryString", queryString);
            SqlDatabase.AddParameter<string>("Session", session);

            int value = (int)SqlDatabase.ExecuteScalarRequest();

            return value;
        }


        private static string GetErrorType(Exception ex)
        {
            return ex.GetType().ToString();
        }

        private static string GetObjectThrownIn(Exception ex)
        {
            MethodInfo methodInfo = GetMethodInfo(ex);
            return methodInfo.Name;
        }

        private static string GetClassName(Exception ex)
        {
            MethodInfo methodInfo = GetMethodInfo(ex);

            return methodInfo.ReflectedType.Name;
        }

        private static string GetPackageName(Exception ex)
        {
            MethodInfo methodInfo = GetMethodInfo(ex);

            return methodInfo.ReflectedType.Namespace;
        }

        private static string GetMethodParameters(Exception ex)
        {
            MethodInfo methodInfo = GetMethodInfo(ex);

            string parameters = "";

            ParameterInfo[] arr = methodInfo.GetParameters();
            bool first = true;
            foreach (ParameterInfo param in arr)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    parameters += ", ";
                }

                string parameterType = param.ParameterType.Name.Trim();

                switch (parameterType)
                {
                    case "String":
                        parameters += "string";
                        break;

                    case "Double":
                        parameters += "double";
                        break;

                    case "Byte":
                        parameters += "byte";
                        break;

                    case "Single":
                        parameters += "float";
                        break;

                    case "Int16":
                        parameters += "short";
                        break;

                    case "Int32":
                        parameters += "int";
                        break;

                    case "Int64":
                        parameters += "long";
                        break;

                    default:
                        parameters += param.ParameterType.Name;
                        break;
                }
                parameters += " " + param.Name;
            }

            return parameters;
        }

        private static MethodInfo GetMethodInfo(Exception ex)
        {
            // extract the stacktrace
            StackTrace st = new StackTrace(ex, true);

            // extract the frame the error was thrown on
            StackFrame frame = st.GetFrame(0);

            // extract and store information about the method with the error
            return (MethodInfo)frame.GetMethod();
        }
    }
}
