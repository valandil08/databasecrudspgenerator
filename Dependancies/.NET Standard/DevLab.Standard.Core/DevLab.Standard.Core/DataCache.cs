﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace DevLab.Standard.Core
{
    public static class DataCache
    {
        private static ConcurrentDictionary<string, DataContainer> cache = new ConcurrentDictionary<string, DataContainer>();

        public static void Set(string name, object value, int validForMinuites)
        {
            DateTime expiryDateTime = DateTime.Now.AddMilliseconds(validForMinuites);
            Set(name, value, expiryDateTime);
        }

        public static void Set(string name, object value, DateTime expiryDateTime)
        {
            if (cache.ContainsKey(name))
            {
                cache[name].SetValue(value, expiryDateTime);
            }
            else
            {
                cache.GetOrAdd(name, new DataContainer(value, expiryDateTime));
            }
        }

        public static bool HasExpired(string name, bool waitIfNull = false)
        {
            if (cache.ContainsKey(name))
            {
                return cache[name].HasExpired();
            }

            return false;
        }

        public static object Get(string name, bool waitIfNull = false)
        {
            if (cache.ContainsKey(name))
            {
                return cache[name].GetValue();
            }

            return null;
        }

        public class DataContainer
        {
            private object value;
            private DateTime expiryDate;

            public DataContainer(object value, DateTime expiryDate)
            {
                this.value = value;
                this.expiryDate = expiryDate;
            }

            public object GetValue()
            {
                object value = null;

                if (expiryDate >= DateTime.Now)
                {
                    value = this.value;
                }

                return value;
            }

            public bool HasExpired()
            {
                return expiryDate > DateTime.Now;
            }

            public void SetValue(object value, DateTime expiryDate)
            {
                this.value = value;
                this.expiryDate = expiryDate;
            }
        }
    }
}
