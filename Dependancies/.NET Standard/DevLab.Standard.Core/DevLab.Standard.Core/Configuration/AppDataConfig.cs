﻿using System;
using System.IO;
using System.Xml;

namespace DevLab.Standard.Core.Configuration
{
    public class AppDataConfig : IConfig
    {
        private string SubFolder = "";
        private const string FileName = "AppConfig.xml";
        private string AppDataUrl = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public AppDataConfig(string subFolder = null)
        {
            if (!string.IsNullOrWhiteSpace(subFolder))
            {
                SubFolder = subFolder + "/";
            }
        }

        public bool DoesAppSettingExist(string key)
        {
            if (!DoesFileExist())
            {
                return false;
            }

            return GetAppSetting(key) != null;
        }

        public bool DoesConnectionStringExist(string key)
        {
            if (!DoesFileExist())
            {
                return false;
            }

            return GetConnectionString(key) != null;
        }

        public string GetAppSetting(string key)
        {
            if (!DoesFileExist())
            {
                return null;
            }

            return GetValue("appsetting", key, "value");            
        }

        public string GetConnectionString(string key)
        {
            if (!DoesFileExist())
            {
                return null;
            }

            return GetValue("connectionStrings", key, "connectionString");
        }

        private string GetValue(string mainTag, string key, string attributeName)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(AppDataUrl + "/" + SubFolder + FileName);

            foreach (XmlElement element in doc)
            {
                if (element.Name.ToLower().Equals(mainTag))
                {
                    foreach (XmlElement appSetting in element.ChildNodes)
                    {
                        if (appSetting.Name.ToLower().Equals("add"))
                        {
                            if (appSetting.HasAttribute("name"))
                            {
                                if (appSetting.HasAttribute(attributeName))
                                {
                                    if (appSetting.GetAttribute("name").Equals(key))
                                    {
                                        return appSetting.GetAttribute(attributeName);
                                    }
                                }
                            }
                        }
                    }
                }
            }


            return null;
        }

        private bool DoesFileExist()
        {
            return File.Exists(AppDataUrl + "/" + SubFolder + FileName);
        }
    }
}
