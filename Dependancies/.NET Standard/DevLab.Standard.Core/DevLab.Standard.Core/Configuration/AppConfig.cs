﻿using DevLab.Standard.Core.Configuration;
using System.Collections.Generic;

namespace System
{
    public static class AppConfig
    {
        private static List<IConfig> configs = new List<IConfig>();

        public static void AddConfig(IConfig config)
        {
            configs.Add(config);
        }

        public static bool DoesAppSettingExist(string key)
        {
            if (configs.Count == 0)
            {
                return GetDefaultConfig().DoesAppSettingExist(key);
            }

            foreach (IConfig config in configs)
            {
                if (config.DoesAppSettingExist(key))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool DoesConnectionStringExist(string key)
        {
            if (configs.Count == 0)
            {
                return GetDefaultConfig().DoesConnectionStringExist(key);
            }

            foreach (IConfig config in configs)
            {
                if (config.DoesConnectionStringExist(key))
                {
                    return true;
                }
            }

            return false;
        }

        public static string GetAppSetting(string key)
        {
            if (configs.Count == 0)
            {
                string value = GetDefaultConfig().GetAppSetting(key);

                if (value != null)
                {
                    return value;
                }
            }

            foreach (IConfig config in configs)
            {
                if (config.DoesAppSettingExist(key))
                {
                    string value = config.GetAppSetting(key);

                    if (value != null)
                    {
                        return value;
                    }
                }
            }

            return null;
        }

        public static string GetConnectionString(string key)
        {
            if (configs.Count == 0)
            {
                string value = GetDefaultConfig().GetConnectionString(key);

                if (value != null)
                {
                    return value;
                }
            }

            foreach (IConfig config in configs)
            {
                if (config.DoesConnectionStringExist(key))
                {
                    string value = config.GetConnectionString(key);

                    if (value != null)
                    {
                        return value;
                    }
                }
            }

            return null;
        }

        private static IConfig GetDefaultConfig()
        {
            return new AppDataConfig();
        }
    }
}