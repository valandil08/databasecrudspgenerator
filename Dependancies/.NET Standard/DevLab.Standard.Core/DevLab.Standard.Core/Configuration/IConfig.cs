﻿namespace DevLab.Standard.Core.Configuration
{
    public interface IConfig
    {
        bool DoesConnectionStringExist(string key);
        bool DoesAppSettingExist(string key);

        string GetAppSetting(string key);
        string GetConnectionString(string key);
    }
}
