﻿using DevLab.Standard.Core.Connections.SQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DevLab.Standard.Integration.DatabaseInfo
{
    public class DbTableInfo : IDbInfo
    {
        public string Schema = "";
        public string Name = "";
        public List<DbColumnInfo> Columns = new List<DbColumnInfo>();
        public List<DbForeignKeyInfo> ForeignKeys = new List<DbForeignKeyInfo>();
        public List<string> UniqueConstraintColumns = new List<string>();

        public DbTableInfo(ISqlDb db, string schema, string name)
        {
            Schema = schema;
            Name = name;

            GetColumns(db, schema, name);
            GetPrimaryKeys(db, schema, name);
            GetIdentityColumns(db, schema, name);
            GetRelations(db, schema, name);
            GetDefulatColumnValues(db, schema, name);
            GetUniqueConstrantColumns(db, schema, name);
        }

        private void GetColumns(ISqlDb db, string schema, string name)
        {
            if (db.GetSqlDbType() != SqlDbEnum.SqlServer)
            {
                throw new Exception("DbDatabaseInfo only works with sql server");
            }

            string sql = "SELECT TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, Is_NULLABLE, ORDINAL_POSITION, COLUMN_DEFAULT, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX, NUMERIC_SCALE, DATETIME_PRECISION FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" + schema + "' AND TABLE_NAME='" + name + "'";
            db.SetSql(sql);

            DataTable dt = db.ExecuteDataTableRequest();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string dataType = row["DATA_TYPE"].ToString();

                    if (row["CHARACTER_MAXIMUM_LENGTH"] != DBNull.Value)
                    {
                        if (row["CHARACTER_MAXIMUM_LENGTH"].ToString().Equals("-1"))
                        {
                            dataType += "(MAX)";
                        }
                        else
                        {
                            dataType += "(" + row["CHARACTER_MAXIMUM_LENGTH"] + ")";
                        }
                    }

                    DbColumnInfo columnInfo = new DbColumnInfo(row["COLUMN_NAME"].ToString(), dataType);

                    if (row["Is_NULLABLE"].ToString().Equals("YES"))
                    {
                        columnInfo.SetIsNullable(true);
                    }

                    if (columnInfo != null)
                    {
                        Columns.Add(columnInfo);
                    }
                }
            }

        }

        private void GetRelations(ISqlDb db, string schema, string name)
        {
            string sql = " SELECT fk.name 'KeyName', cp.name 'ColumnFrom', SCHEMA_NAME(tr.schema_id) 'SchemaTo', tr.name 'TableTo', cr.name 'ColumnTo' FROM sys.foreign_keys fk INNER JOIN sys.tables tp ON fk.parent_object_id = tp.object_id INNER JOIN sys.tables tr ON fk.referenced_object_id = tr.object_id INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id INNER JOIN sys.columns cp ON fkc.parent_column_id = cp.column_id AND fkc.parent_object_id = cp.object_id INNER JOIN sys.columns cr ON fkc.referenced_column_id = cr.column_id AND fkc.referenced_object_id = cr.object_id Where SCHEMA_NAME(tp.schema_id) = '" + schema + "' and tp.name = '" + name + "'";
            db.SetSql(sql);

            DataTable dt = db.ExecuteDataTableRequest();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string keyName = row["KeyName"].ToString();
                    string schemaFrom = schema;
                    string tableFrom = name;
                    string columnFrom = row["ColumnFrom"].ToString();
                    string schemaTo = row["SchemaTo"].ToString();
                    string tableTo = row["TableTo"].ToString();
                    string columnTo = row["ColumnTo"].ToString();

                    ForeignKeys.Add(new DbForeignKeyInfo(keyName, schemaFrom, tableFrom, columnFrom, schemaTo, tableTo, columnTo));
                }
            }

        }

        private void GetPrimaryKeys(ISqlDb db, string schema, string name)
        {
            string sql = " SELECT Col.Column_Name as ColName from INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col WHERE Col.Constraint_Name = Tab.Constraint_Name AND Col.Table_Name = Tab.Table_Name AND Constraint_Type = 'IsIdentity' AND Col.TABLE_SCHEMA = '" + schema + "' AND Col.TABLE_NAME='" + name + "'";
            db.SetSql(sql);

            DataTable dt = db.ExecuteDataTableRequest();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string colName = row["ColName"].ToString();

                    foreach (DbColumnInfo col in Columns)
                    {
                        if (col.Name.Equals(colName))
                        {
                            col.SetIsPrimaryKey(true);
                        }
                    }
                }
            }
        }

        private void GetIdentityColumns(ISqlDb db, string schema, string name)
        {
            string sql = "SELECT NAME AS ColName FROM     SYS.IDENTITY_COLUMNS AS cols WHERE OBJECT_SCHEMA_NAME(OBJECT_ID) = '" + schema + "' AND OBJECT_NAME(OBJECT_ID) ='" + name + "'";
            db.SetSql(sql);

            DataTable dt = db.ExecuteDataTableRequest();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string colName = row["ColName"].ToString();

                    foreach (DbColumnInfo col in Columns)
                    {
                        if (col.Name.Equals(colName))
                        {
                            col.SetIsIdentity(true);
                        }
                    }
                }
            }
        }

        public void GetDefulatColumnValues(ISqlDb db, string schema, string name)
        {
            db.SetSql("SELECT name, object_definition(default_object_id) AS DefaultValue FROM   sys.columns WHERE  object_id = object_id('" + schema + "." + name + "') and object_definition(default_object_id) IS NOT NULL");

            DataTable dt = db.ExecuteDataTableRequest();

            foreach (DbColumnInfo col in Columns)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["name"].ToString().Equals(col.Name))
                    {
                        string defaultValue = row["DefaultValue"].ToString();
                        col.DefaultValue = defaultValue;
                        break;
                    }
                }
            }
        }

        public void GetUniqueConstrantColumns(ISqlDb db, string schema, string name)
        {

            string sql = @" select		CC.Column_Name as 'ColumnName'
                            from		information_schema.table_constraints TC
                            inner join	information_schema.constraint_column_usage CC on 
			                            TC.Constraint_Name = CC.Constraint_Name
                            where		TC.constraint_type = 'Unique' 
                            AND			CC.TABLE_NAME = '" + name + @"' 
                            AND CC.TABLE_SCHEMA = '" + schema + @"'
                            order by    TC.Constraint_Name";

            db.SetSql(sql);

            DataTable dt = db.ExecuteDataTableRequest();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UniqueConstraintColumns.Add(row["ColumnName"].ToString());
                }
            }
        }

        public IDbInfo GenerateFromXml()
        {
            throw new NotImplementedException();
        }

        public DbInfoType GetDbType()
        {
            return DbInfoType.Table;
        }

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<table schema=\"" + Schema + "\" name=\"" + Name + "\">");
            sb.AppendLine("<columns>");
            foreach (DbColumnInfo column in Columns)
            {
                sb.AppendLine(column.ToXml());
            }
            sb.AppendLine("</columns>");
            sb.AppendLine("</table>");

            return sb.ToString();
        }
    }
}