﻿using System;
using System.Data;

namespace DevLab.Standard.Integration.DatabaseInfo
{
    public class DbColumnInfo : IDbInfo
    {
        public string Name;
        public string DataType;
        public bool IsPrimarykey;
        public bool IsIdentity;
        public bool IsNullable;
        public string DefaultValue = null;

        public DbColumnInfo(DataRow row)
        {
            Name = "";
            DataType = "";
        }

        public DbColumnInfo(string name, string dataType)
        {
            Name = name;
            DataType = dataType;
            IsPrimarykey = false;
            IsIdentity = false;
        }

        public void SetIsPrimaryKey(bool value)
        {
            IsPrimarykey = value;
        }

        public void SetIsNullable(bool value)
        {
            IsNullable = value;
        }

        public void SetIsIdentity(bool value)
        {
            IsIdentity = value;
        }

        public IDbInfo GenerateFromXml()
        {
            throw new NotImplementedException();
        }

        public DbInfoType GetDbType()
        {
            return DbInfoType.Column;
        }

        public string ToXml()
        {
            string header = "<column name=\"" + Name + "\" dataType=\"" + DataType + "\"/>";

            return header;
        }
    }
}
