﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab.Standard.Integration.DatabaseInfo
{
    public enum DbInfoType
    {
        Database,
        Table,
        Column,
        StoredProcedure,
        Function,
        View,
        TableRelationship
    }
}
