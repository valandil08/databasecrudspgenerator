﻿namespace DevLab.Standard.Integration.DatabaseInfo
{
    public interface IDbInfo
    {
        IDbInfo GenerateFromXml();
        string ToXml();
        DbInfoType GetDbType();
    }
}
