﻿using DevLab.Standard.Core.Connections.SQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DevLab.Standard.Integration.DatabaseInfo
{
    public class DbDatabaseInfo : IDbInfo
    {
        public string Name;
        public List<DbTableInfo> Tables = new List<DbTableInfo>();

        public DbDatabaseInfo(ISqlDb db, string name)
        {
            if (db.GetSqlDbType() != SqlDbEnum.SqlServer)
            {
                throw new Exception("DbDatabaseInfo only works with sql server");
            }

            Name = name;

            string sql = "SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME NOT IN ( SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS )";
            db.SetSql(sql);

            DataTable dt = db.ExecuteDataTableRequest();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    DbTableInfo tableInfo = new DbTableInfo(db, row["TABLE_SCHEMA"].ToString(), row["TABLE_NAME"].ToString());

                    if (tableInfo != null)
                    {
                        Tables.Add(tableInfo);
                    }
                }
            }
        }

        public IDbInfo GenerateFromXml()
        {
            throw new NotImplementedException();
        }

        public DbInfoType GetDbType()
        {
            return DbInfoType.Database;
        }

        public string ToXml()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<database name=\"" + Name + "\">");
            sb.AppendLine("<tables>");
            foreach (DbTableInfo table in Tables)
            {
                sb.AppendLine(table.ToXml());
            }
            sb.AppendLine("</tables>");
            sb.AppendLine("</database>");

            return sb.ToString();
        }
    }
}
