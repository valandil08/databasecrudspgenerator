﻿using System;

namespace DevLab.Standard.Integration.DatabaseInfo
{
    public class DbForeignKeyInfo : IDbInfo
    {
        public string KeyName { get; set; }
        public string SchemaFrom { get; set; }
        public string TableFrom { get; set; }
        public string ColumnFrom { get; set; }

        public string SchemaTo { get; set; }
        public string TableTo { get; set; }
        public string ColumnTo { get; set; }

        public DbForeignKeyInfo(string keyName, string schemaFrom, string tableFrom, string columnFrom, string schemaTo, string tableTo, string columnTo)
        {
            KeyName = keyName;

            SchemaFrom = schemaFrom;
            TableFrom = tableFrom;
            ColumnFrom = columnFrom;

            SchemaTo = schemaTo;
            TableTo = tableTo;
            ColumnTo = columnTo;
        }

        public IDbInfo GenerateFromXml()
        {
            throw new NotImplementedException();
        }

        public DbInfoType GetDbType()
        {
            return DbInfoType.TableRelationship;
        }

        public string ToXml()
        {
            throw new NotImplementedException();
        }
    }
}
